#!/bin/bash
#PBS -o localhost:${PBS_O_WORKDIR}/
#PBS -e localhost:${PBS_O_WORKDIR}/
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=1
#PBS -q s48

# User defined vars
TMP_JOB_SCRIPT=copy.job.sh

# Create script file for backing up after logining to login node again
create_backup_command() {
    cat << EOF > $TMP_JOB_SCRIPT
rsync -rvP /scratch/wei/scratch.gentoo_root.img /archive/wei/copy.scratch.gentoo_root.img
EOF

}


# ssh to login node and run the stuff
run_script_on_login() {
    ssh login-0-1 'bash -s' < $TMP_JOB_SCRIPT
}

main() {
    echo "Backing up"
    create_backup_command
    run_script_on_login
}

main
